class Calculatrice:

    def __init__(self, n1 =0, n2=0):
        self.set_n1(n1)
        self.set_n2(n2)
    
    def set_n1(self, n1):
        self.__n1 = n1

    def get_n1(self):
        return self.__n1
    
    def set_n2(self, n2):
        self.__n2 = n2

    def get_n2(self):
        return self.__n2
    
    def addition(self,n1, n2):
        return n1 + n2
    
    def soustraction(self,n1, n2):
        return n1 - n2
    
    def multiplication(self,n1, n2):
        return n1 * n2
    
    def division(self,n1,n2):
        if n2 != 0 :
            return n1 / n2 
        return "Impossible de divisier un nombre par 0"
    
calculatrice = Calculatrice()

print(calculatrice.addition(4,2))
print(calculatrice.soustraction(4,2))
print(calculatrice.multiplication(4,2))
print(calculatrice.division(4,2))